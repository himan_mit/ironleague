// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "IronLeagueGameMode.h"
#include "IronLeagueCharacter.h"
#include "Data/AIGlobalParameters.h"
#include "HandBall/HandBall.h"

#include "Engine/World.h"
#include "UObject/ConstructorHelpers.h"
#include "Algo/Transform.h"

AIronLeagueGameMode::AIronLeagueGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}

AIronLeagueGameMode* AIronLeagueGameMode::GetIronLeageGameMode(UObject* WorldContextObject)
{
	if (WorldContextObject)
	{
		if (UWorld *TheWorld = WorldContextObject->GetWorld())
		{
			return Cast<AIronLeagueGameMode>(TheWorld->GetAuthGameMode());
		}
	}

	return nullptr;
}

UAIGlobalParameters* AIronLeagueGameMode::GetAIParameters(UObject* WorldContextObject)
{
	if (AIronLeagueGameMode *GameMode = GetIronLeageGameMode(WorldContextObject))
	{
		return GameMode->AIData;
	}

	return nullptr;
}

AHandBall* AIronLeagueGameMode::GetTheGameBall(UObject* WorldContextObject)
{
	if (AIronLeagueGameMode *GameMode = GetIronLeageGameMode(WorldContextObject))
	{
		return GameMode->GameBall;
	}

	return nullptr;
}

bool AIronLeagueGameMode::RegisterPlayer(AIronLeagueCharacter* Player, const ETeams& Team)
{
	if (Team == ETeams::None)
		return false;

	TArray<TWeakObjectPtr<AIronLeagueCharacter>>& TheTeam = Team == ETeams::Black ? TeamBlack : TeamWhite;
	TArray<TWeakObjectPtr<AIronLeagueCharacter>>& OtherTeam = Team == ETeams::Black ? TeamWhite : TeamBlack;
	if (Player && !TheTeam.Contains(Player) && !OtherTeam.Contains(Player) && TheTeam.Num() < TeamSize)
	{
		TheTeam.Add(Player);
		return true;
	}

	return false;
}

TArray<AIronLeagueCharacter*> AIronLeagueGameMode::GetTeam(const ETeams& Team) const
{
	TArray<TWeakObjectPtr<AIronLeagueCharacter>> PlayerTeam;
	if (Team == ETeams::None)
		PlayerTeam = TeamNone;
	else
		PlayerTeam = Team == ETeams::Black ? TeamBlack : TeamWhite;

	PlayerTeam.RemoveAll([](auto& character) { return !character.IsValid(); });
	TArray<AIronLeagueCharacter*> PlayerTeamTx;
	Algo::Transform(PlayerTeam, PlayerTeamTx, [](auto& character) { return character.Get(); });

	return PlayerTeamTx;
}