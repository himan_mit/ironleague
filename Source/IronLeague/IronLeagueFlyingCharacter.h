// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "IronLeagueCharacter.h"

#include "IronLeagueFlyingCharacter.generated.h"

UENUM(BlueprintType)
enum class EControllerType : uint8
{
	Invalid,
	AI,
	Player
};

DECLARE_DYNAMIC_DELEGATE_OneParam(FOnFlightToTargetComplete, bool, Success);

USTRUCT(BlueprintType)
struct FFlightRequest
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, category = "Flight")
	FVector CurrentTargetLocation = FVector::ZeroVector;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, category = "Flight")
	FOnFlightToTargetComplete FlightRequestComplete;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, category = "Flight")
	float AcceptanceRadius = 100.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, category = "Flight")
	TWeakObjectPtr<AActor> TargetActor;

	bool isActive = false;

	FVector GetCurrentTargetLocation()
	{
		if (TargetActor.IsValid())
		{
			CurrentTargetLocation = TargetActor->GetActorLocation();
		}

		return CurrentTargetLocation;
	}

	void Reset()
	{
		isActive = false;
		CurrentTargetLocation = FVector::ZeroVector;
		FlightRequestComplete.Unbind();
	}
};

UCLASS()
class IRONLEAGUE_API AIronLeagueFlyingCharacter : public AIronLeagueCharacter
{
	GENERATED_BODY()
	
public:
	AIronLeagueFlyingCharacter();

	UPROPERTY(EditDefaultsOnly, category = "Flying")
	float NormalMaxVelocity;
	UPROPERTY(EditDefaultsOnly, category = "Flying")
	float BoostedMaxVelocity = 3500.f;
	UPROPERTY(EditDefaultsOnly, category = "Flying")
	float BoostedAcceleration = 1.7f;
	UPROPERTY(EditDefaultsOnly, category = "Flying")
	float RotationModifier;
	UPROPERTY(EditDefaultsOnly, category = "Flying")
		float RotationSpeed;
	UPROPERTY(EditDefaultsOnly, category = "Flying")
		float VelocityModifierWithBall = 0.8f;

	UPROPERTY(EditDefaultsOnly, category = "Energy")
		float MaxEnergy = 100.f;
	//per second
	UPROPERTY(EditDefaultsOnly, category = "Energy")
		float EnergyRegenRate = 2.f;
	//per second while boosting
	UPROPERTY(EditDefaultsOnly, category = "Energy")
		float EnergyDepleteRateWhileBoosting = 5.f;
	UPROPERTY(EditDefaultsOnly, category = "Energy")
		float EvadeEnergyCost = 20.f;
	UPROPERTY(EditDefaultsOnly, category = "Energy")
		float TackleEnergyCost = 30.f;

	//accelaration in per second
	UPROPERTY(EditDefaultsOnly, category = "Flying")
		float AccelerationRate = 100.f;
	//deaccelaration in per second
	UPROPERTY(EditDefaultsOnly, category = "Flying")
		float DeaccelerationRate = 100.f;

	UPROPERTY(EditDefaultsOnly, Category = "Tackling")
		float TackleTime;
	UPROPERTY(EditDefaultsOnly, Category = "Tackling")
		float TackleDistance;
	UPROPERTY(EditDefaultsOnly, Category = "Tackling")
		float TackleForce;

	UPROPERTY(EditDefaultsOnly, Category = "Evading")
		float EvadeDistance;
	UPROPERTY(EditDefaultsOnly, Category = "Evading")
		float EvadeTime;

	UFUNCTION(BlueprintPure, category = "Flying")
		float GetCurrentVelocity() { return m_CurrentVelocity; }

	UFUNCTION(BlueprintPure, category = "Flying")
	FVector GetCurrentVelocityVector() { return m_CurrentVelocity * m_desiredDirection; }

	/*UFUNCTION(BlueprintNative, category = "Evading")
		void Evading();*/

	EControllerType CurrentControllerType = EControllerType::Invalid;

	UFUNCTION(BlueprintCallable)
	void OnPossesionChanged(class AController *NewController);

	virtual void Tick(float DeltaSeconds);
	virtual void BeginPlay() override;
	virtual void NotifyHit(class UPrimitiveComponent* MyComp, class AActor* Other, class UPrimitiveComponent* OtherComp, bool bSelfMoved, FVector HitLocation, FVector HitNormal, FVector NormalImpulse, const FHitResult& Hit) override;
	

////////////////////////////////////////////////////////////////////
//////AI
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Flight")
	float SweepRadius = 100.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Flight")
	float SweepDistance = 1200.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Flight")
	float TargetReachTolerance = 200.f;

	FFlightRequest CurrentFlightRequest;
	FFlightRequest IntermediateFlightRequest;

	UFUNCTION(BlueprintCallable)
	void FlyToTarget(FFlightRequest FlightRequest);
	
	UFUNCTION(BlueprintCallable)
	void AbortCurrentFlight();

	void TickAI(float DeltaSeconds);

private:

	FFlightRequest& GetFlightToProcess();
	void FinishFlight(bool success, FFlightRequest& FlightToFinish);
	float CalculateTurnSpeed();
	FRotator CalculateAIFlightRotation(float DeltaSeconds);
	float CalculateAIFlightVelocity(float DeltaSeconds);
	void UpdateFlightRequest();
	void PerformFlightCollisionAvoidance(float DeltaSeconds);

//////AI
////////////////////////////////////////////////////////////////////

protected:
	void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	void MoveForward(float value);

	void TurnAtRate(float Rate);
	void LookUpAtRate(float Rate);
	
	void ShootInputPressed();
	void ShootInputReleased();
	void PassInputReleased();
private:
	
	void BoostInputPressed();
	void BoostInputReleased();

	void StartTackling();
	void StopTackling();
	void StartEvading();
	void StopEvading();
	void EvadeRight(float Value);
	void EvadeUp(float Value);
	void Shoot(FVector ShootVector);
	void Pass();

	UPROPERTY(replicated)
	bool m_boosting;
	bool m_flying;
	UPROPERTY(replicated)
		bool m_tackling;
	UPROPERTY(replicated)
	bool m_evading;

	FVector m_evadeDirection;

	float m_MaxVelocity;
	UPROPERTY(replicated)
	float m_CurrentVelocity;
	UPROPERTY(replicated)
		FVector m_desiredDirection;
	UPROPERTY(replicated)
		float m_turnValue;
	UPROPERTY(replicated)
		float m_lookUpValue;
	UPROPERTY(replicated)
	bool m_accelerating;
	//UPROPERTY(replicated)
		float m_currentEnergy;
	UPROPERTY(replicated)
	float m_targetVelocity;

	float m_evadeVelocity;
	float m_tackleVelocity;

	float BallGrabDealyTimer = 0.f;

	FTimerHandle EvadeTimerHandle;
	FTimerHandle TackleTimerHandle;

	UFUNCTION(reliable, server, WithValidation)
		void ServerMoveForward(float Value);

	UFUNCTION(reliable, server, WithValidation)
		void ServerTurnAtRate(float Value);
	UFUNCTION(reliable, server, WithValidation)
		void ServerLookUpAtRate(float Value);

	UFUNCTION(reliable, server, WithValidation)
		void ServerShoot (FVector ShootVector);

	UFUNCTION(reliable, server, WithValidation)
		void ServerPass ();

	UFUNCTION(reliable, server, WithValidation)
		void ServerBoostInputPressed();
	UFUNCTION(reliable, server, WithValidation)
		void ServerBoostInputReleased();
	UFUNCTION(reliable, server, WithValidation)
		void ServerStartTackling();
	UFUNCTION(reliable, server, WithValidation)
		void ServerStopTackling();
	UFUNCTION(reliable, server, WithValidation)
		void ServerStartEvading();
	UFUNCTION(reliable, server, WithValidation)
		void ServerStopEvading();
};
