// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "AIGlobalParameters.generated.h"

UCLASS()
class IRONLEAGUE_API UAIGlobalParameters : public UDataAsset
{
	GENERATED_BODY()

public:

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category=IronLeagueAI)
	float MaxVelocity = 2000.f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = IronLeagueAI)
	float MinTurnSpeed = 70.f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = IronLeagueAI)
	float MaxTurnSpeed = 100.f;
};