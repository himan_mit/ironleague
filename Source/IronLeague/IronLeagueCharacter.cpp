// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "IronLeagueCharacter.h"
#include "HandBall/HandBall.h"

#include "HeadMountedDisplayFunctionLibrary.h"
#include "Camera/CameraComponent.h"
#include "Components/SceneComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/Controller.h"
#include "GameFramework/SpringArmComponent.h"
#include "Engine/World.h"
#include "DrawDebugHelpers.h"
#include "HAl/IConsoleManager.h"
#include "Components/SkeletalMeshComponent.h"
#include "Engine/CollisionProfile.h"
#include "UnrealNetwork.h"

static TAutoConsoleVariable<int32> CVarShowDebugGrabSphere(TEXT("Fly.ShowGrabSphere"), 0, TEXT("Shows/Hides grab debug sphere"));

FName AIronLeagueCharacter::MeshComponentName(TEXT("CharacterMesh0"));
FName AIronLeagueCharacter::CapsuleComponentName(TEXT("CollisionCylinder"));

//////////////////////////////////////////////////////////////////////////
// AIronLeagueCharacter

AIronLeagueCharacter::AIronLeagueCharacter()
{
	CapsuleComponent = CreateDefaultSubobject<UCapsuleComponent>(ACharacter::CapsuleComponentName);
	CapsuleComponent->InitCapsuleSize(34.0f, 88.0f);
	CapsuleComponent->SetCollisionProfileName(UCollisionProfile::Pawn_ProfileName);

	CapsuleComponent->CanCharacterStepUpOn = ECB_No;
	CapsuleComponent->SetShouldUpdatePhysicsVolume(true);
	CapsuleComponent->bCheckAsyncSceneOnMove = false;
	CapsuleComponent->SetCanEverAffectNavigation(false);
	CapsuleComponent->bDynamicObstacle = true;
	RootComponent = CapsuleComponent;


	Mesh = CreateOptionalDefaultSubobject<USkeletalMeshComponent>(ACharacter::MeshComponentName);
	if (Mesh)
	{
		Mesh->AlwaysLoadOnClient = true;
		Mesh->AlwaysLoadOnServer = true;
		Mesh->bOwnerNoSee = false;
		Mesh->MeshComponentUpdateFlag = EMeshComponentUpdateFlag::AlwaysTickPose;
		Mesh->bCastDynamicShadow = true;
		Mesh->bAffectDynamicIndirectLighting = true;
		Mesh->PrimaryComponentTick.TickGroup = TG_PrePhysics;
		Mesh->SetupAttachment(CapsuleComponent);
		static FName MeshCollisionProfileName(TEXT("CharacterMesh"));
		Mesh->SetCollisionProfileName(MeshCollisionProfileName);
		Mesh->SetGenerateOverlapEvents(false);
		Mesh->SetCanEverAffectNavigation(false);
	}

	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate when the controller rotates. Let that just affect the camera.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;
	
	// Create a camera boom (pulls in towards the player if there is a collision)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->TargetArmLength = 300.0f; // The camera follows at this distance behind the character	
	CameraBoom->bUsePawnControlRotation = true; // Rotate the arm based on the controller

	// Create a follow camera
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName); // Attach the camera to the end of the boom and let the boom adjust to match the controller orientation
	FollowCamera->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Note: The skeletal mesh and anim blueprint references on the Mesh component (inherited from Character) 
	// are set in the derived blueprint asset named MyCharacter (to avoid direct content references in C++)

	BallThrower = CreateDefaultSubobject<USceneComponent>(TEXT("BallThrower0"));
	check(BallThrower);
	BallThrower->SetupAttachment(RootComponent);
}

void AIronLeagueCharacter::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	if(PlayerTeam != ETeams::None)
		SetTeam(PlayerTeam);
}

void AIronLeagueCharacter::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	TickTimers(DeltaSeconds);
	SeekBall();

	if (CVarShowDebugGrabSphere.GetValueOnGameThread() > 0)
	{
		::DrawDebugSphere(GetWorld(), GetActorLocation(), GrabRadius, 10, FColor::Blue, false, -1.0f);
	}
}

void AIronLeagueCharacter::TickTimers(float DeltaTime)
{
	// Ball grab timer
	if (BallGrabDealyTimer > 0.f)
	{
		BallGrabDealyTimer = FMath::Clamp(BallGrabDealyTimer - DeltaTime, 0.f, BallGrabDealyTimer);

		if (FMath::Abs(BallGrabDealyTimer) <= KINDA_SMALL_NUMBER)
		{
			BallGrabDealyTimer = 0.f;
			OnBallGrabDelayExpired();
		}
	}

	if (shootHeld)
	{
		shootHeldTime += DeltaTime;
	}
}

void AIronLeagueCharacter::SetBallGrabDelayTimer()
{
	BallGrabDealyTimer = BallGrabDelay;
}

void AIronLeagueCharacter::OnBallGrabDelayExpired() {}

bool AIronLeagueCharacter::CanGrabBall() const
{
	return BallGrabDealyTimer < KINDA_SMALL_NUMBER;
}

void AIronLeagueCharacter::SeekBall()
{
	
		if (GrabbedBall || !CanGrabBall())
		{
			return;
		}

		FCollisionQueryParams QueryParams(TEXT(""), false, this);
		if (UWorld * TheWorld = GetWorld())
		{
			TArray<FOverlapResult> Overlaps;
			TheWorld->OverlapMultiByChannel(Overlaps, GetActorLocation(), FQuat::Identity, ECollisionChannel::ECC_PhysicsBody, FCollisionShape::MakeSphere(GrabRadius), QueryParams);

			int32 Idx = Overlaps.IndexOfByPredicate([](const FOverlapResult& res) { return res.Actor.IsValid() ? res.Actor->GetClass()->IsChildOf(AHandBall::StaticClass()) : false; });
			if (Idx == INDEX_NONE || !GetMesh())
			{
				return;
			}
			AHandBall *TheBall = Cast<AHandBall>(Overlaps[Idx].Actor);

			if (TheBall != nullptr)
			{
				if (Role < ROLE_Authority)
				{
					ServerGrabBall();
				}
				else
				{
					ClientGrabBall();
				}
			}
		}
}

void AIronLeagueCharacter::ClientGrabBall()
{
	UWorld *pWorld = GetWorld();
	if (pWorld)
	{
		AIronLeagueGameMode *pGameMode = (AIronLeagueGameMode*)pWorld->GetAuthGameMode();
		if (pGameMode)
		{
			AHandBall* TheBall = pGameMode->GetTheGameBall(pWorld);
			if (TheBall != nullptr && TheBall->TryGrabbingBall(this))
			{
				GrabbedBall = TheBall;
				GrabbedBall->AttachToComponent(GetMesh(), FAttachmentTransformRules::SnapToTargetNotIncludingScale, TEXT("HandBall"));
			}
		}
	}
}

void AIronLeagueCharacter::ServerGrabBall_Implementation()
{
	ClientGrabBall();
}

bool AIronLeagueCharacter::ServerGrabBall_Validate()
{
	return true;
}

void AIronLeagueCharacter::SetTeam(const ETeams &Team)
{
	bool registeredToTeam = false;
	if (AIronLeagueGameMode * TheMode = AIronLeagueGameMode::GetIronLeageGameMode(this))
	{
		registeredToTeam = TheMode->RegisterPlayer(this, PlayerTeam);
		PlayerTeam = Team;
	}

	if (!registeredToTeam)
		PlayerTeam = ETeams::None;
}

FVector AIronLeagueCharacter::GetPlayerVelocity_Implementation() const
{
	//if (UCharacterMovementComponent *characterMovement = GetCharacterMovement())
	//{
	//	characterMovement->Velocity;
	//}

	return GetVelocity();
}

void AIronLeagueCharacter::PassBall_Implementation()
{
	if (!GrabbedBall || PlayerTeam == ETeams::None)
	{
		return;
	}

	if (AIronLeagueGameMode *GameMode = AIronLeagueGameMode::GetIronLeageGameMode(this))
	{
		const auto &Team = GameMode->GetTeam(PlayerTeam);

		const FVector& currFwd = GetActorForwardVector();
		AIronLeagueCharacter *TheChosenOne = nullptr;
		float minAngle = 1.57f;
		for (const auto &Player : Team)
		{
			if (Player && Player != this)
			{
				FVector toOtherPlayer = Player->GetActorLocation() - GetActorLocation();
				float angle = FMath::Acos(FVector::DotProduct(toOtherPlayer.GetSafeNormal(), currFwd));

				if (angle < minAngle)
				{
					minAngle = angle;
					TheChosenOne = Player;
				}
			}
		}

		if (minAngle >= PassAssistAngle || TheChosenOne == nullptr)
		{
			return;
		}

		PrepareBallThrow();
		GrabbedBall->DetachFromActor(FDetachmentTransformRules::KeepWorldTransform);
		GrabbedBall->PassBall(TheChosenOne);

		GrabbedBall = NULL;
		SetBallGrabDelayTimer();
	}
}

void AIronLeagueCharacter::Shoot_Implementation(FVector ShootVector)
{
	if (!GrabbedBall)
	{
		return;
	}
		
	PrepareBallThrow();

	GrabbedBall->DetachFromActor(FDetachmentTransformRules::KeepWorldTransform);
	GrabbedBall->ShootBall(ShootVector, FVector::ZeroVector);

	GrabbedBall = NULL;
	SetBallGrabDelayTimer();
}

FVector AIronLeagueCharacter::GetCurrentShootVector_Implementation() const
{
	const float fwdPower = ShootFwdPower + FMath::Min((shootHeldTime / ShootMaxPowerTime), 1.f) * (ShootFwdPowerMax - ShootFwdPower);
	const float upPower = ShootUpPower + FMath::Min((shootHeldTime / ShootMaxPowerTime), 1.f) * (ShootUpPowerMax - ShootUpPower);
	return GetActorForwardVector() * fwdPower + GetActorUpVector() * upPower;
}

FVector AIronLeagueCharacter::GetBallThowPosition_Implementation() const
{
	return BallThrower->GetComponentLocation();
}

void AIronLeagueCharacter::PrepareBallThrow()
{
	if (GrabbedBall)
	{
		FTransform ThrowTransform = BallThrower->GetComponentTransform();
		FTransform CurrTransform = GrabbedBall->GetActorTransform();
		ThrowTransform.SetScale3D(CurrTransform.GetScale3D());
		GrabbedBall->SetActorTransform(ThrowTransform);
	}
}

void AIronLeagueCharacter::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(AIronLeagueCharacter, GrabbedBall);
	DOREPLIFETIME(AIronLeagueCharacter, shootHeldTime);
}