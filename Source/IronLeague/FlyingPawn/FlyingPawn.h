// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
#pragma once

#include "BallPlayer/BallPlayer.h"

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "FlyingPawn.generated.h"


UCLASS(Config=Game)
class IRONLEAGUE_API AFlyingPawn : public APawn, public IBallPlayer
{
	GENERATED_BODY()

	/** StaticMesh component that will be the visuals for our flying pawn */
	UPROPERTY(Category = Mesh, VisibleDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class UStaticMeshComponent* PlaneMesh;

	/** Spring arm that will offset the camera */
	UPROPERTY(Category = Camera, VisibleDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* SpringArm;

	/** Camera component that will be our viewpoint */
	UPROPERTY(Category = Camera, VisibleDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* Camera;

	/** Camera component that will be our viewpoint */
	UPROPERTY(Category = Collision, VisibleDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class USphereComponent* ProximitySensorSphere;

	/** How quickly forward speed changes */
	UPROPERTY(Category = Shooting, EditAnywhere)
	float ShootForwardPower = 4000.f;

	/** How quickly forward speed changes */
	UPROPERTY(Category = Shooting, EditAnywhere)
	float ShootVerticalPower = 1000.f;

	/** How quickly forward speed changes */
	UPROPERTY(Category = Speed, EditAnywhere)
	float Acceleration;

	/** How quickly pawn can steer */
	UPROPERTY(Category = Speed, EditAnywhere)
	float VelocityInterpSpeed;

	/** How quickly pawn can steer */
	UPROPERTY(Category = Speed, EditAnywhere)
	float TurnInterpSpeed = 2.f;

	/** How quickly pawn can steer */
	UPROPERTY(Category = Speed, EditAnywhere)
	float MaxTurnSpeed;

	/** How quickly pawn can steer */
	UPROPERTY(Category = Speed, EditAnywhere)
	float MaxPitchSpeed = 0.5f;

	/** Max forward speed */
	UPROPERTY(Category = Speed, EditAnywhere)
	float MaxSpeed;

	/** Min forward speed */
	UPROPERTY(Category = Speed, EditAnywhere)
	float MinSpeed;

	/** Min forward speed */
	UPROPERTY(Category = Speed, EditAnywhere)
	float PermanentTrajectory = false;

	bool ShowTrajectory = false;
	FVector CurrentVelocity  = FVector::ZeroVector;
	FVector CurrentAcceleration = FVector::ZeroVector;
	FVector CurrentInputVector = FVector::ZeroVector;
	float CurrentYawSpeed = 0.f;
	float CurrentPitchSpeed = 0.f;
	TWeakObjectPtr<class AHandBall> BallWithinProximity = nullptr;
public:
	AFlyingPawn();

	// Begin AActor overrides
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaSeconds) override;
	virtual void NotifyHit(class UPrimitiveComponent* MyComp, class AActor* Other, class UPrimitiveComponent* OtherComp, bool bSelfMoved, FVector HitLocation, FVector HitNormal, FVector NormalImpulse, const FHitResult& Hit) override;
	// End AActor overrides

	UFUNCTION()
	void OnObjectEnteredProximity(class UPrimitiveComponent *OverlappedComponent, class AActor *OtherActor, class UPrimitiveComponent *OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult &SweepResult);
	
	UFUNCTION()
	void OnObjectExitProximity(class UPrimitiveComponent* OverlappedComponent, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

// IBallPlayer

	virtual FVector GetPlayerVelocity_Implementation() const override;
	virtual FVector GetCurrentShootVector_Implementation() const override;

// IBallPlayer

	/** Returns PlaneMesh subobject **/
	FORCEINLINE class UStaticMeshComponent* GetPlaneMesh() const { return PlaneMesh; }
	/** Returns SpringArm subobject **/
	FORCEINLINE class USpringArmComponent* GetSpringArm() const { return SpringArm; }
	/** Returns Camera subobject **/
	FORCEINLINE class UCameraComponent* GetCamera() const { return Camera; }

protected:

	UPROPERTY(Category = Ball, EditAnywhere, BlueprintReadOnly)
	TWeakObjectPtr<class AHandBall> GrabbedBall = nullptr;

	// Begin APawn overrides
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override; // Allows binding actions/axes to functions
	// End APawn overrides

	UFUNCTION(BlueprintCallable)
	void MoveFwdInput(float Val);
	
	UFUNCTION(BlueprintCallable)
	void MoveUpInput(float Val);

	UFUNCTION(BlueprintCallable)
	void MoveRightInput(float Val);

	UFUNCTION(BlueprintCallable)
	void RotateYawInput(float Val);

	UFUNCTION(BlueprintCallable)
	void RotatePitchInput(float Val);
	
	UFUNCTION(BlueprintCallable)
	void GrabBallInput();

	UFUNCTION(BlueprintCallable)
	void ReleaseBallInternal();

	UFUNCTION(BlueprintCallable)
	void ShootReleasedInput();

	UFUNCTION(BlueprintCallable)
	void ShootPressedInput();
};
