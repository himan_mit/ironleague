// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "FlyingPawn.h"
#include "HandBall/HandBall.h"

#include "UObject/ConstructorHelpers.h"
#include "Camera/CameraComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Components/SphereComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "Engine/World.h"
#include "Engine/StaticMesh.h"

DEFINE_LOG_CATEGORY_STATIC(LogFlyingPawn, Log, All)

AFlyingPawn::AFlyingPawn()
{
	// Structure to hold one-time initialization
	struct FConstructorStatics
	{
		ConstructorHelpers::FObjectFinderOptional<UStaticMesh> PlaneMesh;
		FConstructorStatics()
			: PlaneMesh(TEXT("/Game/Developers/jumper/Flying/Meshes/UFO.UFO"))
		{
		}
	};
	static FConstructorStatics ConstructorStatics;

	// Create static mesh component
	PlaneMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("PlaneMesh0"));
	check(PlaneMesh);

	PlaneMesh->SetStaticMesh(ConstructorStatics.PlaneMesh.Get());	// Set static mesh
	RootComponent = PlaneMesh;

	// Create a spring arm component
	SpringArm = CreateDefaultSubobject<USpringArmComponent>(TEXT("SpringArm0"));
	check(SpringArm);

	SpringArm->SetupAttachment(RootComponent);	// Attach SpringArm to RootComponent
	SpringArm->TargetArmLength = 160.0f; // The camera follows at this distance behind the character	
	SpringArm->SocketOffset = FVector(0.f,0.f,60.f);
	SpringArm->bEnableCameraLag = false;	// Do not allow camera to lag
	SpringArm->CameraLagSpeed = 15.f;
	
	SpringArm->SetAbsolute(false, false, false);

	// Create camera component 
	Camera = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera0"));
	check(Camera);

	Camera->SetupAttachment(SpringArm, USpringArmComponent::SocketName);	// Attach the camera
	Camera->bUsePawnControlRotation = false; // Don't rotate camera with controller

	ProximitySensorSphere = CreateDefaultSubobject<USphereComponent>(TEXT("Sphere0"));
	check(ProximitySensorSphere);
	ProximitySensorSphere->SetupAttachment(PlaneMesh);

	// Set handling parameters
	Acceleration = 500.f;
	MaxSpeed = 4000.f;
	MinSpeed = 500.f;
}

void AFlyingPawn::BeginPlay()
{
	Super::BeginPlay();

	ProximitySensorSphere->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	ProximitySensorSphere->OnComponentBeginOverlap.AddDynamic(this, &AFlyingPawn::OnObjectEnteredProximity);
	ProximitySensorSphere->OnComponentEndOverlap.AddDynamic(this, &AFlyingPawn::OnObjectExitProximity);
}

void AFlyingPawn::Tick(float DeltaSeconds)
{
	// Call any parent class Tick implementation
	Super::Tick(DeltaSeconds);

	FVector InputDirection = CurrentInputVector.GetSafeNormal();
	float InputSize = CurrentInputVector.Size();

	FVector WorldMoveDirection = GetActorTransform().TransformVectorNoScale(InputDirection).GetSafeNormal();
	FVector AccelerationVector = WorldMoveDirection * InputSize * Acceleration;

	if (InputSize <= KINDA_SMALL_NUMBER)
	{
		AccelerationVector -= Acceleration * 0.5f * CurrentVelocity.GetSafeNormal();
	}

	// Shouldn't be able to set high velocity when blocked
	CurrentVelocity = CurrentVelocity + AccelerationVector * DeltaSeconds;

	CurrentVelocity = FMath::Clamp(CurrentVelocity.Size(), MinSpeed, MaxSpeed) * CurrentVelocity.GetSafeNormal();

	AddActorWorldOffset(CurrentVelocity * DeltaSeconds, true);

	AddActorWorldRotation(FRotator(0.f, CurrentYawSpeed * DeltaSeconds, 0.f));

	FRotator CurrRot = GetActorTransform().GetRotation().Rotator();

	if (FMath::Abs(CurrentPitchSpeed) > KINDA_SMALL_NUMBER)
	{
		CurrRot.Pitch += CurrentPitchSpeed;
	}
	else
	{
		CurrRot.Pitch = FMath::FInterpConstantTo(CurrRot.Pitch, 0.f, DeltaSeconds, TurnInterpSpeed);
	}

	CurrRot.Pitch = FMath::Clamp(CurrRot.Pitch, -40.f, 40.f);

	SetActorRotation(CurrRot);

	CurrentInputVector = FVector::ZeroVector;
}

void AFlyingPawn::NotifyHit(class UPrimitiveComponent* MyComp, class AActor* Other, class UPrimitiveComponent* OtherComp, bool bSelfMoved, FVector HitLocation, FVector HitNormal, FVector NormalImpulse, const FHitResult& Hit)
{
	Super::NotifyHit(MyComp, Other, OtherComp, bSelfMoved, HitLocation, HitNormal, NormalImpulse, Hit);

	if (AHandBall *HandBall = Cast<AHandBall>(Other))
	{
		// Don't bounce back when colliding with ball
		return;
	}

	const FVector CurrVelocityDir = CurrentVelocity.GetSafeNormal();
	float DotProduct = FVector::DotProduct(CurrVelocityDir, -HitNormal);

	FVector CorrectingVel = CurrentVelocity.ProjectOnToNormal(-HitNormal) * (DotProduct + 0.3f); // Modify correcting velocity by angle between hit dir and velocity

	//CurrentVelocity *= 0.8f;   // Reduce the overall velocity by a bit
	CurrentVelocity -= CorrectingVel;   // Reverse velocity along hit normal

	//// Deflect along the surface when we collide.
	//FRotator CurrentRotation = GetActorRotation();

	//FRotator NewRotation = FRotator(CurrentRotation.Pitch, FMath::FInterpTo(CurrentRotation.Yaw, HitNormal.ToOrientationRotator().Yaw, GetWorld()->GetDeltaSeconds(), 50.f), CurrentRotation.Roll);

	//SetActorRotation(NewRotation);
}

void AFlyingPawn::OnObjectEnteredProximity(class UPrimitiveComponent* OverlappedComponent, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (AHandBall * HandBall = Cast<AHandBall>(OtherActor))
	{
		BallWithinProximity = HandBall;
	}
}

void AFlyingPawn::OnObjectExitProximity(class UPrimitiveComponent* OverlappedComponent, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if (AHandBall * HandBall = Cast<AHandBall>(OtherActor))
	{
		BallWithinProximity = nullptr;
	}
}

FVector AFlyingPawn::GetPlayerVelocity_Implementation() const
{
	return CurrentVelocity;
}

FVector AFlyingPawn::GetCurrentShootVector_Implementation() const
{
	return CurrentVelocity + GetActorForwardVector() * ShootForwardPower + GetActorUpVector() * ShootVerticalPower;
}

void AFlyingPawn::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
 //   // Check if PlayerInputComponent is valid (not NULL)
	//check(PlayerInputComponent);

	//// Bind our control axis' to callback functions
	//PlayerInputComponent->BindAxis("MoveFwd", this, &AFlyingPawn::MoveFwdInput);
	//PlayerInputComponent->BindAxis("MoveRight", this, &AFlyingPawn::MoveRightInput);
	//PlayerInputComponent->BindAxis("MoveUp", this, &AFlyingPawn::MoveUpInput);
	//PlayerInputComponent->BindAxis("Turn", this, &AFlyingPawn::RotateYawInput);
	//PlayerInputComponent->BindAxis("TurnVertical", this, &AFlyingPawn::RotatePitchInput);
	//PlayerInputComponent->BindAction("Shoot", EInputEvent::IE_Pressed, this, &AFlyingPawn::ShootPressed);
	//PlayerInputComponent->BindAction("GrabBall", EInputEvent::IE_Released, this, &AFlyingPawn::GrabBall);
	//PlayerInputComponent->BindAction("Shoot", EInputEvent::IE_Released, this, &AFlyingPawn::Shoot);
}

void AFlyingPawn::MoveFwdInput(float Val)
{
	CurrentInputVector += FVector{ Val, 0.f, 0.f};
}

void AFlyingPawn::MoveUpInput(float Val)
{
	CurrentInputVector += FVector{ 0.f, 0.f, Val};
}

void AFlyingPawn::MoveRightInput(float Val)
{
	CurrentInputVector += FVector{ 0.f, Val, 0.f };
}

void AFlyingPawn::RotateYawInput(float Val)
{
	float TargetYawTurn = Val * MaxTurnSpeed;
	CurrentYawSpeed = FMath::FInterpTo(CurrentYawSpeed, TargetYawTurn, GetWorld()->GetDeltaSeconds(), TurnInterpSpeed);
}

void AFlyingPawn::RotatePitchInput(float Val)
{
	float TargetPitchTurn = Val * MaxPitchSpeed;
	CurrentPitchSpeed = FMath::FInterpTo(CurrentPitchSpeed, TargetPitchTurn, GetWorld()->GetDeltaSeconds(), TurnInterpSpeed);
}

void AFlyingPawn::GrabBallInput()
{
	if (!GrabbedBall.IsValid())
	{
		if (BallWithinProximity.IsValid())
		{
			GrabbedBall = BallWithinProximity;
			//GrabbedBall->OnBallGrabbed(this);
			GrabbedBall->AttachToActor(this, FAttachmentTransformRules::SnapToTargetNotIncludingScale, TEXT("HandBall"));
		}
	}
}

void AFlyingPawn::ReleaseBallInternal()
{
	if (GrabbedBall.IsValid())
	{
		GrabbedBall->DetachFromActor(FDetachmentTransformRules::KeepWorldTransform);
		//GrabbedBall->OnBallReleased(GetActorUpVector() * 500.f);
	}

	GrabbedBall = nullptr;
}

void AFlyingPawn::ShootPressedInput()
{
	AHandBall* TheBall = GrabbedBall.Get();
	ReleaseBallInternal();

	FVector ShootVector = IBallPlayer::Execute_GetCurrentShootVector(this);
	if (TheBall)
	{
		//TheBall->StopVisualizingTrajectory();
		//TheBall->ShootBall(ShootVector, FVector::ZeroVector);
	}

	ShowTrajectory = false;
}

void AFlyingPawn::ShootReleasedInput()
{
	if (GrabbedBall.IsValid())
	{
		//GrabbedBall->StartVisualizingTrajectory();
	}
	ShowTrajectory = true;
}