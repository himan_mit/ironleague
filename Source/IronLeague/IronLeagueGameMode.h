// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "IronLeagueGameMode.generated.h"

class AIronLeagueCharacter;
class UAIGlobalParameters;

UENUM(BlueprintType)
enum class ETeams : uint8
{
	None,
	Black,
	White
};

UCLASS(minimalapi)
class AIronLeagueGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AIronLeagueGameMode();

	UFUNCTION(BlueprintCallable)
	bool RegisterPlayer(AIronLeagueCharacter *Player, const ETeams& Team);

	UFUNCTION(BlueprintCallable)
	TArray<AIronLeagueCharacter*> GetTeam(const ETeams& Team) const;
	
	UFUNCTION(BlueprintCallable, meta = (WorldContext = "WorldContextObject"))
	static AIronLeagueGameMode* GetIronLeageGameMode(UObject* WorldContextObject);

	UFUNCTION(BlueprintCallable, meta = (WorldContext = "WorldContextObject"))
	static UAIGlobalParameters* GetAIParameters(UObject* WorldContextObject);

	UFUNCTION(BlueprintCallable, meta = (WorldContext = "WorldContextObject"))
	static class AHandBall* GetTheGameBall(UObject* WorldContextObject);

protected:
	UPROPERTY(Category = Ball, EditAnywhere, BlueprintReadWrite)
	class AHandBall *GameBall;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = AIData)
	UAIGlobalParameters *AIData;

	UPROPERTY(Category = Team, EditAnywhere, BlueprintReadWrite)
	int32 TeamSize = 2;

	TArray<TWeakObjectPtr<AIronLeagueCharacter>> TeamBlack;
	TArray<TWeakObjectPtr<AIronLeagueCharacter>> TeamWhite;

	TArray<TWeakObjectPtr<AIronLeagueCharacter>> TeamNone;
};