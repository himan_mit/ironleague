// Fill out your copyright notice in the Description page of Project Settings.

#include "IronLeagueFlyingCharacter.h"
#include "HandBall/HandBall.h"
#include "Data/AIGlobalParameters.h"

#include "Components/InputComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/Controller.h"
#include "Engine/World.h"
#include "Camera/CameraComponent.h"
#include "DrawDebugHelpers.h"
#include "GameFramework/SpringArmComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "GameFramework/Controller.h"
#include "GameFramework/PlayerController.h"
#include "AIController.h"
#include "Engine.h"
#include "Kismet/KismetMathLibrary.h"
#include "UnrealNetwork.h"

//#TEMP
#pragma optimize("", off)
//#TEMP

AIronLeagueFlyingCharacter::AIronLeagueFlyingCharacter()

	:AIronLeagueCharacter()
{
	m_flying = false;
	m_boosting = false;
	bReplicates = true;
}

void AIronLeagueFlyingCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	check(PlayerInputComponent);

	PlayerInputComponent->BindAxis("MoveForward", this, &AIronLeagueFlyingCharacter::MoveForward);
	PlayerInputComponent->BindAxis("TurnRate", this, &AIronLeagueFlyingCharacter::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUpRate", this, &AIronLeagueFlyingCharacter::LookUpAtRate);
	PlayerInputComponent->BindAction("Boost", IE_Pressed, this, &AIronLeagueFlyingCharacter::BoostInputPressed);
	PlayerInputComponent->BindAction("Boost", IE_Released, this, &AIronLeagueFlyingCharacter::BoostInputReleased);
	PlayerInputComponent->BindAction("Shoot", EInputEvent::IE_Pressed, this, &AIronLeagueFlyingCharacter::ShootInputPressed);
	PlayerInputComponent->BindAction("Shoot", EInputEvent::IE_Released, this, &AIronLeagueFlyingCharacter::ShootInputReleased);
	PlayerInputComponent->BindAction("Tackle", EInputEvent::IE_Pressed, this, &AIronLeagueFlyingCharacter::StartTackling);
	PlayerInputComponent->BindAction("Pass", EInputEvent::IE_Pressed, this, &AIronLeagueFlyingCharacter::PassInputReleased);

	PlayerInputComponent->BindAxis("EvadeUp", this, &AIronLeagueFlyingCharacter::EvadeUp);
	PlayerInputComponent->BindAxis("EvadeRight", this, &AIronLeagueFlyingCharacter::EvadeRight);
	
	m_MaxVelocity = NormalMaxVelocity;
	m_currentEnergy = MaxEnergy;

	m_tackling = false;
	m_evading = false;

	m_tackleVelocity = TackleDistance / TackleTime;
	m_evadeVelocity = EvadeDistance / EvadeTime;
	//GetCharacterMovement()->SetMovementMode(EMovementMode::MOVE_Flying);

	//if (GetCharacterMovement()->bIsActive)
	//	GetCharacterMovement()->Deactivate();
}


void AIronLeagueFlyingCharacter::ServerBoostInputPressed_Implementation()
{
	BoostInputPressed();
}

void AIronLeagueFlyingCharacter::ServerBoostInputReleased_Implementation()
{
	BoostInputReleased();
}

bool AIronLeagueFlyingCharacter::ServerBoostInputPressed_Validate()
{
	return true;
}

bool AIronLeagueFlyingCharacter::ServerBoostInputReleased_Validate()
{
	return false;
}

void AIronLeagueFlyingCharacter::BoostInputPressed()
{
	if (Role < ROLE_Authority)
	{
		ServerBoostInputPressed();
	}
	else
	m_boosting = true;
}

void AIronLeagueFlyingCharacter::BoostInputReleased()
{
	if (Role < ROLE_Authority)
	{
		ServerBoostInputReleased();
	}
	else
	m_boosting = false;
}

void AIronLeagueFlyingCharacter::MoveForward(float Value)
{
	if (Role < ROLE_Authority)
	{
		ServerMoveForward(Value);
	}
	else
	{
		if (m_tackling || m_evading)
			return;

 		m_targetVelocity = (m_boosting && Value > 0 ? BoostedMaxVelocity : NormalMaxVelocity * Value) * (GrabbedBall ? VelocityModifierWithBall : 1);
		m_accelerating = m_CurrentVelocity < m_targetVelocity;
	}
}

void AIronLeagueFlyingCharacter::TurnAtRate(float Rate)
{
	if (Role < ROLE_Authority)
	{
		ServerTurnAtRate(Rate);
	}
	else
	{
		m_turnValue = Rate;
	}
}

void AIronLeagueFlyingCharacter::LookUpAtRate(float Rate)
{
	if (Role < ROLE_Authority)
	{
		ServerLookUpAtRate(Rate);
	}
	else
	{
		m_lookUpValue = Rate;
	}
}

void AIronLeagueFlyingCharacter::ServerTurnAtRate_Implementation(float Rate)
{
	TurnAtRate(Rate);
}

bool AIronLeagueFlyingCharacter::ServerTurnAtRate_Validate(float Rate)
{
	return true;
}

void AIronLeagueFlyingCharacter::ServerLookUpAtRate_Implementation(float Rate)
{
	LookUpAtRate(Rate);
}

bool AIronLeagueFlyingCharacter::ServerLookUpAtRate_Validate(float Rate)
{
	return true;
}

void AIronLeagueFlyingCharacter::ShootInputPressed()
{
	shootHeldTime = 0.f;
	shootHeld = true;
	if (GrabbedBall)
	{
		GrabbedBall->StartVisualizingTrajectory();
	}
}

void AIronLeagueFlyingCharacter::ShootInputReleased()
{
	if (GrabbedBall)
	{
		GrabbedBall->StopVisualizingTrajectory();
	}
	FVector ShootVector = IBallPlayer::Execute_GetCurrentShootVector(this);
	Shoot(ShootVector);
	shootHeld = false;
	shootHeldTime = 0.f;
}

void AIronLeagueFlyingCharacter::PassInputReleased()
{
	if (GrabbedBall)
	{
		GrabbedBall->StopVisualizingTrajectory();
	}

	IBallPlayer::Execute_PassBall(this);
}

void AIronLeagueFlyingCharacter::Pass()
{
	if (Role < ROLE_Authority)
	{
		ServerPass();
	}
	else
	{
		IBallPlayer::Execute_PassBall(this);
	}
}

void AIronLeagueFlyingCharacter::ServerPass_Implementation()
{
	Pass();
}

bool AIronLeagueFlyingCharacter::ServerPass_Validate()
{
	return true;
}

void AIronLeagueFlyingCharacter::Shoot(FVector ShootVector)
{
	if (Role < ROLE_Authority)
	{
		ServerShoot(ShootVector);
	}
	else
	{
		IBallPlayer::Execute_Shoot(this, ShootVector);
	}
}

void AIronLeagueFlyingCharacter::ServerShoot_Implementation(FVector ShootVector)
{
	Shoot(ShootVector);
}

bool AIronLeagueFlyingCharacter::ServerShoot_Validate(FVector ShootVector)
{
	return true;
}

void AIronLeagueFlyingCharacter::OnPossesionChanged(AController* NewController)
{
	if (NewController)
	{
		if (NewController->GetClass()->IsChildOf(APlayerController::StaticClass()))
		{
			CurrentControllerType = EControllerType::Player;
		}
		else if (NewController->GetClass()->IsChildOf(AAIController::StaticClass()))
		{
			CurrentControllerType = EControllerType::AI;
		}
		else
		{
			CurrentControllerType = EControllerType::Invalid;
		}
	}
}

void AIronLeagueFlyingCharacter::NotifyHit(class UPrimitiveComponent* MyComp, class AActor* Other, class UPrimitiveComponent* OtherComp, bool bSelfMoved, FVector HitLocation, FVector HitNormal, FVector NormalImpulse, const FHitResult& Hit)
{
	Super::NotifyHit(MyComp, Other, OtherComp, bSelfMoved, HitLocation, HitNormal, NormalImpulse, Hit);

	// Deflect along the surface when we collide.
	FRotator CurrentRotation = GetActorRotation();
	SetActorRotation(FQuat::Slerp(CurrentRotation.Quaternion(), HitNormal.ToOrientationQuat(), 0.025f));
}

void AIronLeagueFlyingCharacter::BeginPlay()
{
	Super::BeginPlay();
}
#pragma optimize( "", off )
void AIronLeagueFlyingCharacter::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	//if (Role == ROLE_Authority)
	//{
	//	ClientTick(DeltaSeconds);
	//	return;
	//}
	if (CurrentControllerType == EControllerType::AI)
	{
		TickAI(DeltaSeconds);
		return;
	}
	UWorld *pWorld = GetWorld();

	if (!pWorld)
		return;

	if (m_evading || m_tackling)
	{
		FVector moveDirection = m_evading ? m_evadeDirection : (m_tackling ? GetActorForwardVector() : FVector::ZeroVector);
		float velocity = m_evading ? m_evadeVelocity : (m_tackling ? m_tackleVelocity : 0);

		FVector moveDelta = moveDirection * DeltaSeconds * velocity;
		AddActorWorldOffset(moveDelta, true);
		//rotate while evading
		if (m_evading)
		{
			//FQuat currentRotation = GetActorQuat();
			//SetActorRotation(GetActorRightVector().RotateAngleAxis(DeltaSeconds * 360.f / EvadeTime, GetActorForwardVector()).Rotation());
			//FVector::Rotate
			//FQuat newRotation = currentRotation + 
			//SetActorRotation(FQuat::Slerp(currentRotation, FQuat()
		}
	}
	else
	{

		m_currentEnergy = FMath::Clamp(m_currentEnergy +
			((m_accelerating && m_boosting) ? -EnergyDepleteRateWhileBoosting : EnergyRegenRate) * DeltaSeconds,
			0.f, MaxEnergy);

		bool canBoost = m_currentEnergy > 0 && m_boosting;

		float acceleration = m_accelerating ? (canBoost ? BoostedAcceleration : AccelerationRate) : -DeaccelerationRate;

		m_CurrentVelocity = FMath::Clamp(m_CurrentVelocity + acceleration * DeltaSeconds, 0.f, m_accelerating ? m_targetVelocity : m_CurrentVelocity);

		m_desiredDirection = GetActorForwardVector() + GetActorRightVector() * RotationModifier * m_turnValue + GetActorUpVector() * m_lookUpValue * RotationModifier;
		m_desiredDirection.Normalize();

		FVector moveDelta = m_desiredDirection * DeltaSeconds * m_CurrentVelocity;

		AddActorWorldOffset(moveDelta, true);

		GetCameraBoom()->SetRelativeRotation(FQuat::Slerp(GetActorForwardVector().Rotation().Quaternion(), m_desiredDirection.Rotation().Quaternion(), DeltaSeconds * RotationSpeed));
		SetActorRotation(FQuat::Slerp(GetActorForwardVector().Rotation().Quaternion(), m_desiredDirection.Rotation().Quaternion(), DeltaSeconds * RotationSpeed));

	}
	GEngine->AddOnScreenDebugMessage(-1, 2 * pWorld->GetDeltaSeconds(), FColor::Red, FString::Printf(TEXT("current fly speed = %f, current energy = %f"), m_CurrentVelocity, m_currentEnergy));

}
#pragma optimize( "", on )


////////////////////////////////////////////////////////////////////
//////AI

void AIronLeagueFlyingCharacter::FlyToTarget(FFlightRequest FlightRequest)
{
	if (CurrentControllerType != EControllerType::AI)
	{
		return;
	}

	CurrentFlightRequest = FlightRequest;
	CurrentFlightRequest.isActive = true;

	// Reset intermediate request
	IntermediateFlightRequest.Reset();

	UpdateFlightRequest();
}

void AIronLeagueFlyingCharacter::AbortCurrentFlight()
{
	FinishFlight(false, IntermediateFlightRequest);
	FinishFlight(false, CurrentFlightRequest);
}

void AIronLeagueFlyingCharacter::TickAI(float DeltaSeconds)
{
	if (CurrentFlightRequest.isActive)
	{
		if (CurrentFlightRequest.GetCurrentTargetLocation().Equals(GetActorLocation(), CurrentFlightRequest.AcceptanceRadius))
		{
			// Finish both intermediate and main flights
			FinishFlight(true, IntermediateFlightRequest);
			FinishFlight(true, CurrentFlightRequest);
		}
		else
		{
			if (IntermediateFlightRequest.isActive)
			{
				// zero means invalid
				if (IntermediateFlightRequest.GetCurrentTargetLocation().IsNearlyZero())
				{
					FinishFlight(false, IntermediateFlightRequest);
				}
				else if (IntermediateFlightRequest.GetCurrentTargetLocation().Equals(GetActorLocation(), IntermediateFlightRequest.AcceptanceRadius))
				{
					FinishFlight(true, IntermediateFlightRequest);
				}
			}

			PerformFlightCollisionAvoidance(DeltaSeconds);
			FRotator FinalRotation = CalculateAIFlightRotation(DeltaSeconds);

			m_desiredDirection = FinalRotation.Vector().GetSafeNormal();
			m_CurrentVelocity = CalculateAIFlightVelocity(DeltaSeconds);
		}
	}
	else
	{
		m_CurrentVelocity = FMath::FInterpTo(m_CurrentVelocity, 0.f, DeltaSeconds, 50.f);

		FVector vectorToProject = FVector::ForwardVector;
		if (FVector::DotProduct(FVector::ForwardVector, m_desiredDirection) < FVector::DotProduct(-FVector::ForwardVector, m_desiredDirection))
		{
			vectorToProject = -FVector::ForwardVector;
		}

		FVector stableFwd = FVector::VectorPlaneProject(vectorToProject, GetActorRightVector());
		m_desiredDirection = FMath::VInterpTo(m_desiredDirection, stableFwd, DeltaSeconds, 50.f);
	}

	SetActorRotation(m_desiredDirection.Rotation());
	AddActorWorldOffset(m_desiredDirection * m_CurrentVelocity * DeltaSeconds, true);
}

FFlightRequest& AIronLeagueFlyingCharacter::GetFlightToProcess()
{
	return IntermediateFlightRequest.isActive  ? IntermediateFlightRequest : CurrentFlightRequest;
}

void AIronLeagueFlyingCharacter::FinishFlight(bool success, FFlightRequest &FlightToFinish)
{
	if (CurrentFlightRequest.isActive)
	{
		FlightToFinish.FlightRequestComplete.ExecuteIfBound(success);
		FlightToFinish.Reset();
	}
}

float AIronLeagueFlyingCharacter::CalculateTurnSpeed()
{
	float minTurnSpeed = 60.f;
	float maxTurnSpeed = 100.f;
	float maxVelocity = 1000.f;
	if (UAIGlobalParameters *AIData = AIronLeagueGameMode::GetAIParameters(this))
	{
		minTurnSpeed = AIData->MinTurnSpeed;
		maxTurnSpeed = AIData->MaxTurnSpeed;
		maxVelocity = AIData->MaxVelocity;
	}

	return minTurnSpeed + (1.f - (m_CurrentVelocity / maxVelocity)) * (maxTurnSpeed - minTurnSpeed);
}

FRotator AIronLeagueFlyingCharacter::CalculateAIFlightRotation(float DeltaSeconds)
{
	FFlightRequest& FlightToProcess = GetFlightToProcess();
	const FVector toTargetDir = (FlightToProcess.GetCurrentTargetLocation() - GetActorLocation()).GetSafeNormal();
	const FRotator basetargetRotation = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), FlightToProcess.GetCurrentTargetLocation());

	return FMath::RInterpConstantTo(GetActorRotation(), basetargetRotation, DeltaSeconds, CalculateTurnSpeed());
}

float AIronLeagueFlyingCharacter::CalculateAIFlightVelocity(float DeltaSeconds)
{
	FFlightRequest& FlightToProcess = GetFlightToProcess();
	float MaxVelocity = 1000.f;
	if (UAIGlobalParameters *AIData = AIronLeagueGameMode::GetAIParameters(this))
	{
		MaxVelocity = AIData->MaxVelocity;
	}

	float turnSpeed = CalculateTurnSpeed();

	const FVector &displacementToTarget = FlightToProcess.GetCurrentTargetLocation() - GetActorLocation();
	const FVector &fwd = GetActorForwardVector().GetSafeNormal();
	const FRotator TargetRotation = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), FlightToProcess.GetCurrentTargetLocation());
	
	float remainingRotation = FMath::Acos(FVector::DotProduct(fwd, TargetRotation.Vector().GetSafeNormal()));
	
	// Check if we are almost at the target orientation, Just prevent division by zero
	if (FMath::IsNearlyZero(remainingRotation, 0.2f))
	{
		remainingRotation = KINDA_SMALL_NUMBER;
	}

	const float maxTurnArc = displacementToTarget.Size() * 0.5f * remainingRotation;

	// We need not worry about division by small number as velocity wouldn't exceed MaxVelocity
	const float maxConstVelocity = maxTurnArc * FMath::DegreesToRadians(turnSpeed) / remainingRotation;

	const float velocityScalar = FMath::Clamp(1.f - (remainingRotation / PI), 0.2f, 1.f);
	
	float retVel = FMath::FInterpTo(m_CurrentVelocity, MaxVelocity * velocityScalar, DeltaSeconds, 45.f);
	retVel = FMath::Clamp(retVel, 0.f, maxConstVelocity);

	return retVel;
}

void AIronLeagueFlyingCharacter::UpdateFlightRequest()
{
	FCollisionQueryParams QParams(NAME_None, false);
	QParams.AddIgnoredActor(this);
	QParams.AddIgnoredActor(AIronLeagueGameMode::GetTheGameBall(this));

	FFlightRequest& FlightRequestToProcess = GetFlightToProcess();
	FHitResult hitRes;
	FVector start = FlightRequestToProcess.GetCurrentTargetLocation() + 1.f;
	FVector end = FlightRequestToProcess.GetCurrentTargetLocation() - 300.f;

	if(GetWorld()->LineTraceSingleByChannel(hitRes, start, end, ECollisionChannel::ECC_Camera, QParams))
	{
		if (hitRes.Actor.IsValid() && !hitRes.Actor->IsRootComponentMovable())
		{
			// Target slightly above ground to avoid collision
			FlightRequestToProcess.CurrentTargetLocation = FlightRequestToProcess.GetCurrentTargetLocation() + FVector(0.f, 0.f, 200.f);
		}
	}
}

void AIronLeagueFlyingCharacter::PerformFlightCollisionAvoidance(float DeltaSeconds)
{
	if (!CurrentFlightRequest.isActive || !GetWorld())
	{
		return;
	}

	FCollisionQueryParams QParams(NAME_None, false);
	QParams.AddIgnoredActor(this);
	QParams.AddIgnoredActor(AIronLeagueGameMode::GetTheGameBall(this));
	QParams.AddIgnoredActor(CurrentFlightRequest.TargetActor.Get());
	// Ignore collisions with goal posts??

	FHitResult hitRes;
	FVector start = GetActorLocation() + (CurrentFlightRequest.GetCurrentTargetLocation() - GetActorLocation()).GetSafeNormal() * 60.f;
	FVector end = start + (CurrentFlightRequest.GetCurrentTargetLocation() - start).GetSafeNormal() * SweepDistance;

	//::DrawDebugSphere(GetWorld(), (start + end) / 2.f, SweepRadius, 10, FColor::Red, false, -1.0f);

	if (GetWorld()->SweepSingleByChannel(hitRes, start, end, FQuat::Identity, ECollisionChannel::ECC_Camera, FCollisionShape::MakeSphere(SweepRadius), QParams))
	{
		if (AIronLeagueFlyingCharacter *flyer = Cast<AIronLeagueFlyingCharacter>(hitRes.Actor))
		{
			FVector flyerVel = flyer->GetCurrentVelocityVector();
			
			//Check if the flyer is moving towards the sphere
			if (FVector::DotProduct(hitRes.ImpactNormal, flyerVel.GetSafeNormal()) < 0.35f)
			{
				float offset = SweepDistance * 0.5f;
				FVector NewFlyTarget = flyer->GetActorLocation() - flyerVel.GetSafeNormal() * offset;
				
				if (flyerVel.SizeSquared() < FMath::Square(60.f))
				{
					FVector rightVec = GetActorRightVector();
					NewFlyTarget = flyer->GetActorLocation() + offset * rightVec;
				}

				float MaxVelocity = 1500.f;
				if (UAIGlobalParameters * AIData = AIronLeagueGameMode::GetAIParameters(this))
				{
					MaxVelocity = AIData->MaxVelocity;
				}

				// Start reducing its speed
				if(m_CurrentVelocity > MaxVelocity * 0.65f)
					m_CurrentVelocity = m_CurrentVelocity * DeltaSeconds * 0.6f;

				IntermediateFlightRequest.isActive = true;
				IntermediateFlightRequest.GetCurrentTargetLocation() = NewFlyTarget;

				// Try to finish the main flight before intermediate
				IntermediateFlightRequest.AcceptanceRadius = CurrentFlightRequest.AcceptanceRadius / 2.f;

				UpdateFlightRequest();
			}
			else
			{
				// Path is clear
				IntermediateFlightRequest.Reset();
			}
		}
		else if(hitRes.Actor.IsValid())
		{
			//FVector currPos = GetActorLocation();
			//FBox bb = hitRes.Actor->GetComponentsBoundingBox(false);
			//b

			/*FBox box()

			FVector XYprojectedExtents = FVector::VectorPlaneProject(extents, FVector::UpVector);
			XYprojectedExtents.Z = currPos.Z;
		
			FVector NewFlyTarget = XYprojectedExtents;
			if(FVector::DistSquared(currPos, XYprojectedExtents) > FMath::FVector::Distance*/

		}
	}
	else
	{
		// Path is clear
		IntermediateFlightRequest.Reset();
	}
}

//////AI
////////////////////////////////////////////////////////////////////

void AIronLeagueFlyingCharacter::StartTackling()
{
	m_tackleVelocity = TackleDistance / TackleTime;

	if (Role < ROLE_Authority)
	{
		if (m_currentEnergy >= TackleEnergyCost)
		{
			m_currentEnergy -= TackleEnergyCost;
			ServerStartTackling();
		}
	}
	else
	{
		if (!m_evading && !m_tackling)
		{
			m_tackling = true;
			GetWorld()->GetTimerManager().SetTimer(TackleTimerHandle, this, &AIronLeagueFlyingCharacter::StopTackling, TackleTime);
		}
	}
}

void AIronLeagueFlyingCharacter::StopTackling()
{
	if (Role < ROLE_Authority)
	{
		ServerStopTackling();
	}
	else
	m_tackling = false;
}

void AIronLeagueFlyingCharacter::ServerStartTackling_Implementation()
{
	StartTackling();
}

void AIronLeagueFlyingCharacter::ServerStopTackling_Implementation()
{
	StopTackling();
}

bool AIronLeagueFlyingCharacter::ServerStartTackling_Validate()
{
	return true;
}

bool AIronLeagueFlyingCharacter::ServerStopTackling_Validate()
{
	return true;
}

void AIronLeagueFlyingCharacter::StartEvading()
{

	m_evadeVelocity = EvadeDistance / EvadeTime;
	if (Role < ROLE_Authority)
	{
		if (m_currentEnergy >= EvadeEnergyCost)
		{
			m_currentEnergy -= EvadeEnergyCost;
			ServerStartTackling();
		}
	}
	else
	{
		if (!m_tackling && !m_evading)
		{
			m_evading = true;
			if (InputComponent)
			{
				m_evadeDirection = GetActorUpVector() * InputComponent->GetAxisValue("EvadeUp")
					+ GetActorRightVector() * InputComponent->GetAxisValue("EvadeRight");
				m_evadeDirection.Normalize();
				GetWorld()->GetTimerManager().SetTimer(EvadeTimerHandle, this, &AIronLeagueFlyingCharacter::StopEvading, EvadeTime);
			}
		}
	}
}

void AIronLeagueFlyingCharacter::StopEvading()
{
	if (Role < ROLE_Authority)
	{
		ServerStopTackling();
	}
	else
	m_evading = false;
}

void AIronLeagueFlyingCharacter::ServerStartEvading_Implementation()
{
	StartEvading();
}

void AIronLeagueFlyingCharacter::ServerStopEvading_Implementation()
{
	StopEvading();
}

bool AIronLeagueFlyingCharacter::ServerStartEvading_Validate()
{
	return true;
}

bool AIronLeagueFlyingCharacter::ServerStopEvading_Validate()
{
	return true;
}

void AIronLeagueFlyingCharacter::EvadeRight(float Value)
{
	if(Value != 0)
		StartEvading();
}

void AIronLeagueFlyingCharacter::EvadeUp(float Value)
{
	if (Value != 0)
		StartEvading();
}

void AIronLeagueFlyingCharacter::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(AIronLeagueFlyingCharacter, m_CurrentVelocity);
	DOREPLIFETIME(AIronLeagueFlyingCharacter, m_desiredDirection);
	DOREPLIFETIME(AIronLeagueFlyingCharacter, m_tackling);
	DOREPLIFETIME(AIronLeagueFlyingCharacter, m_evading);
	DOREPLIFETIME(AIronLeagueFlyingCharacter, m_turnValue);
	DOREPLIFETIME(AIronLeagueFlyingCharacter, m_lookUpValue);
	DOREPLIFETIME(AIronLeagueFlyingCharacter, m_accelerating);
	DOREPLIFETIME(AIronLeagueFlyingCharacter, m_targetVelocity);
}

bool AIronLeagueFlyingCharacter::ServerMoveForward_Validate(float value)
{
	return true;
}

void AIronLeagueFlyingCharacter::ServerMoveForward_Implementation(float value)
{
	MoveForward(value);
}