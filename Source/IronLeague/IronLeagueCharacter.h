// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "BallPlayer/BallPlayer.h"
#include "IronLeagueGameMode.h"

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "IronLeagueCharacter.generated.h"

UCLASS(config=Game)
class AIronLeagueCharacter : public APawn, public IBallPlayer
{
	GENERATED_BODY()

protected:
	/** Follow camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* FollowCamera;
	/** Camera boom positioning the camera behind the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	/** The main skeletal mesh associated with this Character (optional sub-object). */
	UPROPERTY(Category = Character, VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
		USkeletalMeshComponent* Mesh;

	/** The CapsuleComponent being used for movement collision (by CharacterMovement). Always treated as being vertically aligned in simple collision check functions. */
	UPROPERTY(Category = Character, VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
		UCapsuleComponent* CapsuleComponent;

	/** Component that holds ball just before throwing*/
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USceneComponent* BallThrower;
public:
	AIronLeagueCharacter();

	virtual void PostInitializeComponents() override;

	virtual void Tick(float DeltaSeconds);

	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns FollowCamera subobject **/
	FORCEINLINE class UCameraComponent* GetFollowCamera() const { return FollowCamera; }

// IBallPlayer
	FVector GetPlayerVelocity_Implementation() const override;
	void PassBall_Implementation();
	void Shoot_Implementation(FVector ShootVector) override;
	FVector GetCurrentShootVector_Implementation() const override;
	FVector GetBallThowPosition_Implementation() const override;
// IBallPlayer

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Team")
	ETeams PlayerTeam = ETeams::None;

	/** Returns Mesh subobject **/
	class USkeletalMeshComponent* GetMesh() const { return Mesh; }

	/** Name of the MeshComponent. Use this name if you want to prevent creation of the component (with ObjectInitializer.DoNotCreateDefaultSubobject). */
	static FName MeshComponentName;

	/** Returns CapsuleComponent subobject **/
	class UCapsuleComponent* GetCapsuleComponent() const { return CapsuleComponent; }

	/** Name of the CapsuleComponent. */
	static FName CapsuleComponentName;

protected:
	void TickTimers(float DeltaTime);
	void SetBallGrabDelayTimer();
	void OnBallGrabDelayExpired();
	bool CanGrabBall() const;
	void SeekBall();
	//UFUNCTION(NetMulticast, reliable)
	void ClientGrabBall();
	void PrepareBallThrow();  // Prepare for pass/shoot

	UFUNCTION(BlueprintCallable)
	void SetTeam(const ETeams& Team);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shooting")
	float ShootFwdPower = 5000.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shooting")
	float ShootFwdPowerMax = 8000.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shooting")
	float ShootUpPower = 900.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shooting")
	float ShootUpPowerMax = 2500.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shooting")
	float ShootMaxPowerTime = 1.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shooting")
	float GrabRadius = 500.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ball Handle")
	float BallGrabDelay = 1.f;

	UPROPERTY(Category = Pass, EditAnywhere, BlueprintReadWrite)
	float PassAssistAngle = 0.75f;

	float BallGrabDealyTimer = 0.f;

	UPROPERTY(Category = "Shooting", EditAnywhere, BlueprintReadWrite, replicated)
	float shootHeldTime = 0.f;

	UPROPERTY(Category = "Shooting", EditAnywhere, BlueprintReadWrite)
	bool shootHeld = false;

	UPROPERTY(Category = Ball, VisibleAnywhere, BlueprintReadOnly, replicated)
	AHandBall* GrabbedBall;

private:
	UFUNCTION(reliable, server, WithValidation)
	void ServerGrabBall();
};

