// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "TimerManager.h"
#include "HandBall.generated.h"

UCLASS()
class IRONLEAGUE_API AHandBall : public APawn
{
	GENERATED_BODY()

	UPROPERTY(Category = Trajectory, VisibleDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class UDecalComponent* ImpactDecal;

	UPROPERTY(Category = Mesh, VisibleDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class UStaticMeshComponent* BallMesh;

	UPROPERTY(Category = Trajectory, VisibleDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class UInstancedStaticMeshComponent* TrajectoryMesh;

	void DisplayTrajectory();
	void EndPass();
	void TickPass(float DeltaSeconds);

	float gravityZ = 980.f;
	bool canBeGrabbed = true;
	float passAssistTimer = 0.f;
	float linearDampingBeforePass = 0.3f;
	FTimerHandle TrajectoryUpdateTimerHandle;
	FVector ReceiverInitialPos = FVector::ZeroVector;
	UPROPERTY(replicated)
	AActor* HoldingPlayer = nullptr;
	UPROPERTY(replicated)
	AActor* PassReceiver = nullptr;
public:
	// Sets default values for this pawn's properties
	AHandBall();

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UFUNCTION(BlueprintCallable)
	void SetSimulatePhysics(const bool bSimulate);

	UFUNCTION(BlueprintCallable)
	bool IsSimulatingPhysics() const;

	UFUNCTION(BlueprintCallable)
	void ShootBall(const FVector &LaunchVelocity, const FVector &GoalPos);

	UFUNCTION(BlueprintCallable)
	void PassBall(class AActor* Receiver);

	UFUNCTION(BlueprintCallable)
	bool TryGrabbingBall(class AActor *GrabbingActor);

	UFUNCTION(BlueprintCallable)
	void StartVisualizingTrajectory();

	UFUNCTION(BlueprintCallable)
	void StopVisualizingTrajectory();

	UFUNCTION(BlueprintCallable, BlueprintPure)
	FVector CalculatePassLaunchVelocity() const;

	UFUNCTION(BlueprintCallable)
	class AActor* GetBallHolder();

protected:

	UPROPERTY(Category = Pass, EditAnywhere, BlueprintReadWrite /*,meta = (ClampMin = 0, ClampMax = 1, UIMin = 0, UIMax = 1)*/)
	float PassAssistance = 50.f;

	UPROPERTY(Category = Pass, EditAnywhere, BlueprintReadWrite)
	float PassAssistTolerance = 250.f;

	UPROPERTY(Category = Pass, EditAnywhere, BlueprintReadWrite)
	float PassAssistDelay = 0.4f;

	// Base pass launch angle in degress
	UPROPERTY(Category = Pass, EditAnywhere, BlueprintReadWrite)
	float BasePassAngle = 5.f;

	UPROPERTY(Category = Pass, EditAnywhere, BlueprintReadWrite /*,meta = (ClampMin = 0, ClampMax = 1, UIMin = 0, UIMax = 1)*/)
	float GrabInvulnerabilityTime = 0.25f;

	// Update per seconds
	UPROPERTY(Category = Trajectory, EditAnywhere, BlueprintReadWrite)
	float TrajectoryUdpateFrequency = 25.f;

	UPROPERTY(Category = Trajectory, EditAnywhere, BlueprintReadWrite)
	int32 NumTrajectoryElements = 10;

	UPROPERTY(Category = Trajectory, EditAnywhere, BlueprintReadWrite)
	float MaxSimTime = 6.f;
	
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	virtual void NotifyHit(class UPrimitiveComponent* MyComp, class AActor* Other, class UPrimitiveComponent* OtherComp, bool bSelfMoved, FVector HitLocation, FVector HitNormal, FVector NormalImpulse, const FHitResult& Hit) override;

	UFUNCTION(BlueprintCallable)
	void OnBallReleased(const FVector& ReleaseVelocity);
};
