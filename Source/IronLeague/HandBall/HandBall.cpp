// Fill out your copyright notice in the Description page of Project Settings.


#include "HandBall.h"
#include "BallPlayer/BallPlayer.h"

#include "Components/StaticMeshComponent.h"
#include "Components/InstancedStaticMeshComponent.h"
#include "Components/DecalComponent.h"
#include "Engine/Classes/Kismet/GameplayStatics.h"
#include "DrawDebugHelpers.h"
#include "Engine/World.h"
#include "TimerManager.h"
#include "UnrealNetwork.h"

// #TEMP Remove this
#pragma optimize("", off)
// #TEMP

void AHandBall::DisplayTrajectory()
{
	UWorld* TheWorld = GetWorld();
	IBallPlayer* BallPlayer = Cast<IBallPlayer>(HoldingPlayer);

	if (!TheWorld || !BallPlayer)
	{
		return;
	}

	float normLinearDamping = BallMesh->GetLinearDamping();
	const int32 nSamples = 60;	// TODO : Get the locked frame rate

	FVector CurrVel = IBallPlayer::Execute_GetCurrentShootVector(HoldingPlayer);
	FVector CurrPos = IBallPlayer::Execute_GetBallThowPosition(HoldingPlayer);

	const float StepDeltaTime = MaxSimTime / nSamples;

	// TODO : Do collision checks only against wall / goal post / floors / ceiling? // Create a collision channel

	bool hit = false;
	FHitResult TraceHit(NoInit);

	TArray<FVector> Path;
	Path.Reserve(nSamples + 1);

	Path.Add(CurrPos);
	float CurrentTime = 0.f;
	while (CurrentTime < MaxSimTime)
	{
		// Integrate (Velocity Verlet method)
		FVector OldVelocity = CurrVel;
		FVector LastPos = CurrPos;
		CurrVel += FVector(0.f, 0.f, gravityZ * StepDeltaTime);
		CurrVel *= FMath::Max((1.f - normLinearDamping * StepDeltaTime), 0.f);    // Damping seems to be frame rate dependent, so Step delta time should be similar to physx deltatime for accuracy
		CurrPos += (OldVelocity + CurrVel) * (0.5f * StepDeltaTime);

		FCollisionQueryParams QueryParams(NAME_None /*SCENE_QUERY_STAT(PredictProjectilePath) */, false);
		QueryParams.AddIgnoredActors(TArray<AActor*>{ this });

		hit = TheWorld->SweepSingleByChannel(TraceHit, LastPos, CurrPos, FQuat::Identity, ECollisionChannel::ECC_Visibility, FCollisionShape::MakeSphere(50.f), QueryParams);

		if (hit)
		{
			break;
		}

		Path.Add(CurrPos);

		CurrentTime += StepDeltaTime;
	}

	const int32 nTrajectoryElements = FMath::Max(FMath::Min(Path.Num(), NumTrajectoryElements), 1);
	
	TArray<FVector> PathVisualSamples;
	PathVisualSamples.Reserve(nTrajectoryElements);

	const int32 VisualSampleGap = Path.Num() / nTrajectoryElements;
	for (int32 Idx = VisualSampleGap; Idx < Path.Num(); Idx += VisualSampleGap)
	{
		FVector &CurrPathPos = Path[Idx];
		PathVisualSamples.Add(CurrPathPos);
	}

	if (!hit)
	{
		// TODO  : Log Something is wrong. We should always hit something
	}

	if (TrajectoryMesh)
	{
		const FVector& Scale = TrajectoryMesh->GetComponentTransform().GetScale3D();

		if (PathVisualSamples.Num())
		{
			FVector PrevSample = GetActorLocation();
			for (int32 Idx = 0; Idx < PathVisualSamples.Num(); ++Idx)
			{
				FRotator Rotation = (PathVisualSamples[Idx] - PrevSample).ToOrientationRotator();
				Rotation.Pitch = 90.f;
				FTransform InstanceTx(Rotation, PathVisualSamples[Idx], Scale);
				if (TrajectoryMesh->GetInstanceCount() > Idx)
				{
					TrajectoryMesh->UpdateInstanceTransform(Idx, InstanceTx, true, false, true);
				}
				else
				{
					TrajectoryMesh->AddInstanceWorldSpace(InstanceTx);
				}

				PrevSample = PathVisualSamples[Idx];
			}
		}

		// Hide unnneded instance meshes
		for (int32 Idx = PathVisualSamples.Num(); Idx < TrajectoryMesh->GetInstanceCount(); ++Idx)
		{
			FTransform InstanceTx = FTransform::Identity;
			InstanceTx.SetScale3D(FVector(0.f));
			TrajectoryMesh->UpdateInstanceTransform(Idx, InstanceTx, true, false, true);
		}

		// Mark render state as dirty for instant update
		if (TrajectoryMesh->GetInstanceCount() > 0)
		{
			const int32 LastIdx = TrajectoryMesh->GetInstanceCount() - 1;
			FTransform InstTx = FTransform::Identity;
			TrajectoryMesh->GetInstanceTransform(LastIdx, InstTx, true);

			TrajectoryMesh->UpdateInstanceTransform(LastIdx, InstTx, true, true, true);
		}
	}

	// Show the impact decal at impact point
	if (ImpactDecal)
	{
		if (hit && TraceHit.bBlockingHit)
		{
			FRotator Rotation = TraceHit.ImpactNormal.ToOrientationRotator();
			FTransform DecalTx = FTransform(Rotation, TraceHit.ImpactPoint);
			ImpactDecal->SetWorldTransform(DecalTx);
			ImpactDecal->SetVisibility(true, false);
		}
		else
		{
			ImpactDecal->SetVisibility(false, false);
		}
	}

	//::DrawDebugSphere(GetWorld(), CurrPos, 50.f, 12, FColor::Green, false, 20.f);

	//float hVel0 = FMath::Sqrt(FMath::Square(LaunchVel.X) + FMath::Square(LaunchVel.Y));
	//float vertVel0 = LaunchVel.Z;

	//FVector planeNormal = FVector::DotProduct(LaunchVel.GetSafeNormal(), FVector::UpVector).GetSafeNormal();
	//FPlane projectilePlane = FPlane(LaunchPos, planeNormal);

	//const float GravitySqr = GravityZ * GravityZ;
	//for (float Time = 0.f; Time <= SimTime; Time += SimTime / nSamples)
	//{
	//	const float TimeSqr = Time * Time;
	//	float hDist = hVel0 * Time;
	//	float vertDist = vertVel0 * Time - 0.5f * GravityZ * TimeSqr;

	//	::DrawDebugSphere(GetWorld(), CurrPos, 20.f, 12, FColor::Green, false, 20.f);
	//}

	//FPredictProjectilePathResult res;

	//FPredictProjectilePathParams Params;
	//Params.LaunchVelocity = CurrVel;
	//Params.DrawDebugTime = 20.0f;
	//Params.DrawDebugType = EDrawDebugTrace::ForDuration;
	//Params.LaunchVelocity = CurrVel;
	//Params.MaxSimTime = 5.f;
	//Params.ProjectileRadius = 20.f;
	//Params.SimFrequency = 15;
	//Params.StartLocation = CurrPos;
	//UGameplayStatics::PredictProjectilePath(this, Params, res);
}

void AHandBall::EndPass()
{
	ReceiverInitialPos = FVector::ZeroVector;
	passAssistTimer = 0.f;

	// Reset passing state
	if (PassReceiver)
	{
		BallMesh->SetLinearDamping(linearDampingBeforePass);
		PassReceiver = NULL;
	}
}

// Sets default values
AHandBall::AHandBall()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	BallMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("BallBody"));
	check(BallMesh);

	SetRootComponent(BallMesh);

	TrajectoryMesh = CreateDefaultSubobject<UInstancedStaticMeshComponent>(TEXT("TrajectoryMesh0"));
	if (TrajectoryMesh)
	{
		TrajectoryMesh->SetupAttachment(RootComponent);
	}

	ImpactDecal = CreateDefaultSubobject<UDecalComponent>(TEXT("ImpactDecal0"));
	if (ImpactDecal)
	{
		ImpactDecal->SetupAttachment(RootComponent);
	}
}

void AHandBall::SetSimulatePhysics(const bool bSimulate)
{
	if (IsSimulatingPhysics() == bSimulate)
		return;

	BallMesh->SetSimulatePhysics(bSimulate);
}

bool AHandBall::IsSimulatingPhysics() const
{
	return BallMesh->IsSimulatingPhysics();
}

void AHandBall::ShootBall(const FVector& LaunchVelocity, const FVector& GoalPos)
{
	OnBallReleased(FVector::ZeroVector);
	if (!IsSimulatingPhysics())
	{
		SetSimulatePhysics(true);
	}

	BallMesh->SetPhysicsLinearVelocity(LaunchVelocity);
}

void AHandBall::PassBall(AActor *Receiver)
{
	PassReceiver = Receiver;
	linearDampingBeforePass = BallMesh->GetLinearDamping();
	FVector LaunchVelocity = CalculatePassLaunchVelocity();

	OnBallReleased(FVector::ZeroVector);
	if (!IsSimulatingPhysics())
	{
		SetSimulatePhysics(true);
	}

	ReceiverInitialPos = Receiver ? Receiver->GetActorLocation() : FVector::ZeroVector;
	passAssistTimer = PassAssistDelay;
	BallMesh->SetLinearDamping(0.f);
	BallMesh->SetPhysicsLinearVelocity(LaunchVelocity);
}

bool AHandBall::TryGrabbingBall(class AActor* GrabbingActor)
{
	if (HoldingPlayer || !canBeGrabbed)
	{
		return false;
	}

	HoldingPlayer = GrabbingActor;
	BallMesh->SetPhysicsLinearVelocity(FVector::ZeroVector);
	
	if (IsSimulatingPhysics())
	{
		SetSimulatePhysics(false);
	}

	BallMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	EndPass();
	return true;
}

void AHandBall::OnBallReleased(const FVector &ReleaseVelocity)
{
	canBeGrabbed = false;
	if (UWorld *TheWorld = GetWorld())
	{
		FTimerHandle TimerHandle;
		TheWorld->GetTimerManager().SetTimer(TimerHandle, FTimerDelegate::CreateLambda([this]() { canBeGrabbed = true; }), GrabInvulnerabilityTime, false);
	}

	BallMesh->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);

	if (!IsSimulatingPhysics())
	{
		SetSimulatePhysics(true);
	}

	BallMesh->SetPhysicsLinearVelocity(ReleaseVelocity);
	HoldingPlayer = NULL;
}

// Called when the game starts or when spawned
void AHandBall::BeginPlay()
{
	Super::BeginPlay();

	if (UWorld * TheWorld = GetWorld())
	{
		gravityZ = TheWorld->GetGravityZ();
	}

	if (ImpactDecal)
	{
		ImpactDecal->SetVisibility(false, false);
	}
}

void AHandBall::NotifyHit(class UPrimitiveComponent* MyComp, class AActor* Other, class UPrimitiveComponent* OtherComp, bool bSelfMoved, FVector HitLocation, FVector HitNormal, FVector NormalImpulse, const FHitResult& Hit)
{
	Super::NotifyHit(MyComp, Other, OtherComp, bSelfMoved, HitLocation, HitNormal, NormalImpulse, Hit);

	EndPass();
}

// Called every frame
void AHandBall::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	TickPass(DeltaTime);
}

void AHandBall::TickPass(float DeltaSeconds)
{
	passAssistTimer = FMath::Clamp(PassAssistTolerance - DeltaSeconds, 0.f, passAssistTimer);

	if (PassReceiver 
		&& passAssistTimer == 0.f
		&& PassAssistance > 0.f
		&& (PassReceiver->GetActorLocation() - ReceiverInitialPos).SizeSquared() >  FMath::Square(PassAssistTolerance))
	{
		const FVector receiverVel = IBallPlayer::Execute_GetPlayerVelocity(PassReceiver);
		const FVector receiverPredicatedLocation = PassReceiver->GetActorLocation() + receiverVel * DeltaSeconds;
		const FVector toReceiver = (receiverPredicatedLocation - GetActorLocation());

		const FVector ballVelDir = BallMesh->GetPhysicsLinearVelocity().GetSafeNormal();
		const float dot = FVector::DotProduct(ballVelDir, toReceiver.GetSafeNormal());
		
		// Ignore if obtuse, its too late
		if (dot > SMALL_NUMBER && dot <= 0.9f)
		{
			// Remove component along balls' current velocity
			FVector compAlongVel = toReceiver.ProjectOnToNormal(ballVelDir);
			FVector force = PassAssistance * (toReceiver - compAlongVel) * DeltaSeconds;
			BallMesh->AddForce(force, NAME_None, true);
		}
	}
}

// Called to bind functionality to input
void AHandBall::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
}

void AHandBall::StartVisualizingTrajectory()
{
	if (UWorld * TheWorld = GetWorld())
	{
		if (TrajectoryUpdateTimerHandle.IsValid())
		{
			TheWorld->GetTimerManager().ClearTimer(TrajectoryUpdateTimerHandle);
		}

		TheWorld->GetTimerManager().SetTimer(TrajectoryUpdateTimerHandle, this, &AHandBall::DisplayTrajectory, 1.f / TrajectoryUdpateFrequency, true, 0.f);
	}
}

void AHandBall::StopVisualizingTrajectory()
{
	if (TrajectoryMesh)
	{
		TrajectoryMesh->ClearInstances();
	}

	if (ImpactDecal)
	{
		ImpactDecal->SetVisibility(false, false);
	}

	if (UWorld *TheWorld = GetWorld())
	{
		if (TrajectoryUpdateTimerHandle.IsValid())
		{
			TheWorld->GetTimerManager().ClearTimer(TrajectoryUpdateTimerHandle);
		}
	}
}

FVector AHandBall::CalculatePassLaunchVelocity() const
{
	const float PassUpPower = 1000.f;
	const float PassFwdPower = 5000.f;

	FVector PassVector = FVector::ZeroVector;
	if (!PassReceiver)
	{
		PassVector = IBallPlayer::Execute_GetCurrentShootVector(HoldingPlayer);
		if (PassVector.IsNearlyZero())
		{
			PassVector = GetActorForwardVector() * PassFwdPower + GetActorUpVector() * PassUpPower;
		}
	}
	else
	{
		FVector BallPos = GetActorLocation();
		FVector ReceiverPos = PassReceiver->GetActorLocation();
		FVector ReceiverVelocity = IBallPlayer::Execute_GetPlayerVelocity(PassReceiver);

		// TODO : Calculate the point at which projectile hits the moving receiver
		//FVector PredictedReceiverPosAfterPassTime = ReceiverPos + (ReceiverVelocity * 0.6f); 
		
		const FVector &toReceiver = ReceiverPos - BallPos;
		const FVector& hDisplacement = { toReceiver.X, toReceiver.Y, 0.f };

		// Ignore sign of the dot product, We are not expecting obtuse angles
		float receiverAngleWithBall = FMath::Abs(FMath::Acos(FVector::DotProduct(toReceiver.GetSafeNormal(), hDisplacement.GetSafeNormal())));

		if (toReceiver.Z < 0.f)
		{
			receiverAngleWithBall = -receiverAngleWithBall;
		}

		const float PassAngle = BasePassAngle + FMath::RadiansToDegrees(receiverAngleWithBall);

		const float y0 = BallPos.Z - ReceiverPos.Z;  // Launch height relative to receiver height
		const float h0 = hDisplacement.Size();  // Horizontal displacement

		float intermediateRes = 0.5f * FMath::Abs(gravityZ) / (h0 * FMath::Tan(FMath::DegreesToRadians(PassAngle)) + y0);
		float v0 = FMath::Sqrt(intermediateRes) * h0 / FMath::Cos(FMath::DegreesToRadians(PassAngle));
		v0 = FMath::Clamp(v0, 1000.f, 10000.f);

		FVector rotAxis = FVector::CrossProduct(FVector::UpVector, hDisplacement.GetSafeNormal()).GetSafeNormal();

		FVector passDir = toReceiver.GetSafeNormal();
		if (!rotAxis.IsNearlyZero())
		{
			passDir = hDisplacement.RotateAngleAxis(-PassAngle, rotAxis).GetSafeNormal();
		}

		PassVector = passDir * v0;
	}

	return PassVector;
}

class AActor* AHandBall::GetBallHolder()
{
	return HoldingPlayer;
}

void AHandBall::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(AHandBall, HoldingPlayer);
	DOREPLIFETIME(AHandBall, PassReceiver);
}