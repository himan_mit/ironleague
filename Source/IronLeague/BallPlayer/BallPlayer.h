// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "BallPlayer.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UBallPlayer : public UInterface
{
	GENERATED_BODY()
};

class IRONLEAGUE_API IBallPlayer
{
	GENERATED_BODY()

public:

	UFUNCTION(BlueprintNativeEvent,BlueprintCallable)
	FVector GetPlayerVelocity() const;

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	int32 GetTeamID() const;

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	FVector GetCurrentShootVector() const;

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	FVector GetBallThowPosition() const;

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	void ReleaseBall();

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	void Shoot(FVector ShootVector);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	void PassBall();
};